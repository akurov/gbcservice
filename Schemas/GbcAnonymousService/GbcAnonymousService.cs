 namespace BPMSoft.Configuration.GbcAnonymousServiceNamespace
{
    using System;
	using System.Collections.Generic;
    using System.ServiceModel;
    using System.ServiceModel.Web;
    using System.ServiceModel.Activation;
    using BPMSoft.Core;
    using BPMSoft.Web.Common;
    using BPMSoft.Core.Entities; 

    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class GbcAnonymousService: BaseService
    {
        private SystemUserConnection _systemUserConnection;
        private SystemUserConnection SystemUserConnection {
            get {
                return _systemUserConnection ?? (_systemUserConnection = (SystemUserConnection)AppConnection.SystemUserConnection);
            }
        }
		
		[OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
        public string GetAccountByName(string Query){
            SessionHelper.SpecifyWebOperationIdentity(HttpContextAccessor.GetInstance(), SystemUserConnection.CurrentUser);
			var result = "Not found";
			if (string.IsNullOrWhiteSpace(Query)) {
				return result;
			}
            var esq = new EntitySchemaQuery(SystemUserConnection.EntitySchemaManager, "Account");
            var colId = esq.AddColumn("Id");
            var colName = esq.AddColumn("Name");
            var esqFilter = esq.CreateFilterWithParameters(FilterComparisonType.Contain, "Name", Query);
            esq.Filters.Add(esqFilter);
            var entities = esq.GetEntityCollection(SystemUserConnection);
            if (entities.Count > 0)
            {
				result = "Count: " + entities.Count.ToString() + ", First item name: \"" + entities[0].GetColumnValue(colName.Name).ToString() + "\"";
            }
            return result;
        }
    }
}